##  Aim
<br>
To study DSB amplitude modulation and determine its modulation factor and the total power in sidebands.
<br>

##  Procedure
1.	Students are asked to select the proper components required to rig up the circuit.
2.	Students will click on the connect button to connect the circuit.
3.	Students will be asked to enter the message and carrier signal.
4.	Students will click on CRO visual to generate the graph.
5.	Students will interpret Vmax and Vmin from the modulated graph and give the input.
6.	The simulator will calculate and display the values of modulating index,power of carrier signal, power in sideband and total power of the modulated wave.
7.	Students will click on Test your Skill tab to test the knowledge they have gained during the experiment.<br>
<b>*Note: If any problem occurs during the experiment the student can refresh the page by clicking on Reset tab.</b>
<br>

##  Theory
<b> Amplitude  modulation:</b>   Modulation  is  a  process  of  translating  information  signal  from low  band  frequency  to  high  band  frequency  that suits  the  transmission  medium.  Information signal  is  usually  of  low  frequency,  so  it  cannot  travel  far.  It needs  a  carrier  signal  of 
  higher frequency  for  long  distance  destination.The  inputs  are  carrier  and  information  (modulating) signals while the output is called the modulated signal. Amplitude Modulation (AM) refers to the modulation technique where the carrier’s amplitude is varied in accordance to the instantaneous value of the modulating or basebandsignal’s amplitude.
  An AM signal is represented as:<br><br><br>
  Sinusoidal carrier wave C(t)given as:<br>
  <div align="center"><b> c(t)=A cos wct</b> <br>             A = Max amplitude of the carrierwave<br>
                                      Wc = carrier frequency <br><br><br></div>
AM wave can be expressed as:<br>
<div align="center"><b>s(t) = x(t) cos wt + A cos wt<br>
s(t) = [A + x(t) ] cos wt<br></b><br><br><br></div>
<b>Modulation Index:</b> The amount by which the amplitude of the carrier wave increases and decreases depends on the amplitude of the information signal 
and is known as modulation index or  depth  of  modulation.  The  extent  of  amplitude  variation  in  AM  about  a  unmodulated  carrier amplitude is measured in terms of a factor called 
 modulation index.<br>
 <div align="center"> <b>m(modulation index) =((Vmax-Vmin)/(Vmax+ Vmin))*100%<br></div>
<br><br>
